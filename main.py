import threading
from multiprocessing import Process, Queue
import time

import camera as camera
import check_config as checkconfig
import http_listener as httplistener
import read_all_configs


URL = "http://10.100.102.197:1880/zones/config"

cameras = []

def main(url):
    global cameras
    configs = read_all_configs.get_jsonparsed_data(url)
    try:
        for config in configs["configuration"]:
            if (checkconfig.check(config)):
                queue = Queue()
                video = Process(target=camera.watch, args=(config, False, queue))
                cameras.append({"name":config["name"], "thread":video, "queue":queue, "config":config, "mqttServer":config["mqttServer"], "mqttTopic":config["mqttTopic"]})
                video.start()
            else:
                print("Error in config " + config["name"])
    except KeyError:
        print("Bad config")

main(URL)

stop_event = threading.Event()
server = threading.Thread(target=httplistener.listen, args=(cameras, 8000))
server.start()

try:
    while True:
        time.sleep(30)
        if not server.is_alive():
            server = threading.Thread(target=httplistener.listen, args=(cameras, 8000))
            server.start()
except KeyboardInterrupt:
    run_http = False



# time.sleep(60)

# for camera in cameras:
#    stop = camera["stop"]
#    stop.set()
