from multiprocessing import Process, Queue
from http.server import BaseHTTPRequestHandler, HTTPServer

import camera as cameraproces
import check_config as check_config
import simplejson
import send


# This class will handle any incoming request from
# a browser
class myHandler(BaseHTTPRequestHandler):

    global cameras

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    # Handler for the GET requests
    def do_GET(self):
        print   ('Get request received')
        components = self.path.split('/')
        components = list(filter(None, components))

        body = '{"error":"id not found"}'

        if self.path == '/':
            body = '{"error":"Service unknown"}'

        elif components[0] == "status":
            for camera in cameras:
                if camera["name"] == components[1]:
                    body = '{"name":'+ camera["name"] + ', "alive":' + str(camera["thread"].is_alive())  + '}'

        elif components[0] == "config":
            for camera in cameras:
                if camera["name"] == components[1]:
                    body = str(camera["config"])

        elif components[0] == "list":
            body = '{"cameras":['
            first  = True
            for camera in cameras:
                if not first:
                    body += ','
                first = False
                body += '{"name":'+ camera["name"] + ', "alive":' + str(camera["thread"].is_alive())  + '}'
            body += ']}'

        elif components[0] == "stop":
            for camera in cameras:
                if camera["name"] == components[1]:
                    if camera["thread"].is_alive():
                        camera["thread"].terminate()
                        body = '{"name":' + camera["name"] + ', "alive":"Stopping"}'
                        for zone in camera["config"]["zones"]:
                            send.send(zone["name"], "Camera Stopping", camera["mqttServer"], camera["mqttTopic"])
                    else:
                        body = '{"name":' + camera["name"] + ', "alive":' + str(camera["thread"].is_alive()) + '}'

        elif components[0] == "start":
            for camera in cameras:
                if camera["name"] == components[1]:
                    if not camera["thread"].is_alive():
                        video = Process(target=cameraproces.watch, args=(camera["config"], True, camera["queue"]))
                        camera["thread"] = video
                        video.start()
                        # send.send(camera["name"], "Camera Sarting", camera["mqttServer"], camera["mqttTopic"])
                    body = '{"name":' + camera["name"] + ', "alive":' + str(camera["thread"].is_alive()) + '}'

        else:
            body = '{"error":"Service unknown"}'

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the html message
        self.wfile.write(body.encode())
        return

    def do_POST(self):
        type = self.headers['Content-Type']
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)
        try:
            config = simplejson.loads(post_data)
        except simplejson.scanner.JSONDecodeError:
            self.send_error(400,"Cannot parse JSON")
            return
        try:
            config = config["configuration"][0]
        except KeyError:
            self.send_error(400, '{"Error":"Wrong config"}')
            return


        if not check_config.check(config):
            self.send_error(400, '{"Error":"Wrong config"}')
            return

        new_camera = True

        for camera in cameras:
            if camera["name"] == config["name"]:
                queue = Queue()
                video = Process(target=cameraproces.watch, args=(config, True, queue))
                camera["thread"] = video
                camera["queue"] = queue
                camera["mqttServer"] = config["mqttServer"]
                camera["mqttTopic"] = config["mqttTopic"]
                video.start()
                new_camera = False
                # send.send(camera["name"], "Camera Sarting", camera["mqttServer"], camera["mqttTopic"])

        if new_camera:
            queue = Queue()
            video = Process(target=cameraproces.watch, args=(config, True, queue))
            cameras.append({"name": config["name"], "thread": video, "queue": queue, "config": config, "mqttServer":config["mqttServer"], "mqttTopic":config["mqttTopic"]})
            video.start()
            # send.send(config["name"], "Camera Sarting", config["mqttServer"], config["mqttTopic"])

        self.send_response(200, "OK")
        self.end_headers()
        return

def listen(list, PORT_NUMBER = 8080):

    global cameras

    cameras = list

    try:
        # Create a web server and define the handler to manage the
        # incoming request
        server = HTTPServer(('', PORT_NUMBER), myHandler)
        print ('Started httpserver on port ' , PORT_NUMBER)
        # Wait forever for incoming http requests
        server.serve_forever()

    except KeyboardInterrupt:
        print ('^C received, shutting down the web server')
        server.socket.close()