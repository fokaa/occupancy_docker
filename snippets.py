import simplejson
from urllib.request import urlopen

URL = "http://10.100.102.197:1880/zones/config"

def get_jsonparsed_data(url):
    response = urlopen(url)
    data = str(response.read())
    data = data[data.find('{'):data.rfind('}')+1]
    try:
        config = simplejson.loads(data)
    except simplejson.scanner.JSONDecodeError:
        print ("Cannot parse JSON")
        return
    return config

config = get_jsonparsed_data(URL)
print (config)