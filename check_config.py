# import traceback

def check(config):
    try:
        name = config["name"]
        config['width'] = int(config['width'])
        config['height'] = int(config['height'])
        config["threshold"] = float(config["threshold"])
        config["skipFrames"] = int(config["skipFrames"])
        URI = config["streamURI"],
        mqtt = config["mqttServer"]
        topic = config["mqttTopic"]
        config["open_kernel"] = int(config["open_kernel"])
        config["close_kernel"] = int(config["close_kernel"])
        for zone in config["zones"]:
            name = zone["name"]
            for point in zone["points"]:
                point[0] = int(point[0])
                point[1] = int(point[1])
            zone["occupy"] = int(zone["occupy"])
            zone["free"] = int(zone["free"])
    except (KeyError, ValueError):
        # traceback.print_exc()
        return False

    return True
