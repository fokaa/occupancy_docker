import simplejson
import socket
from urllib.request import urlopen

def ping(url):
    url = url.split('/')
    url = [component for component in url if component not in ["","http:","https:"]]
    url = url[0].split(':')
    try:
        url = (url[0],int(url[1]))
    except KeyError:
        url = (url[0], 80)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(5)
    try:
        s.connect(url)
    except socket.error:
        return False
    s.close()
    return True


def get_jsonparsed_data(url):

    if ping(url):
        response = urlopen(url)
        data = str(response.read())
        data = data[data.find('{'):data.rfind('}')+1]
        try:
            config = simplejson.loads(data)
        except simplejson.scanner.JSONDecodeError:
            print ("Cannot parse JSON")
            return {}

        return config
    return {}