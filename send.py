import paho.mqtt.publish as publish

def send(zone, message, server, topic):
    payload = '{"Zone":"' + zone + '", "Status":"' + message + '"}'
    # print(server)
    if (server == ""):
        print(payload)
    else:
        publish.single(topic, payload=payload, hostname=server,
               port=1883, client_id="occupancy")
