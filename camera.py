import cv2
import numpy as np
import json

import send
import check_config

rundirectly = False


def preparation(config):
    width = config['width']
    height = config['height']
    thresholdContour = config["threshold"]
    skipFames = config["skipFrames"]
    streamURI = config["streamURI"]
    mqttServer = config["mqttServer"]
    mqttTopic = config["mqttTopic"]
    o_kernel = config["open_kernel"]
    c_kernel = config["close_kernel"]
    zonesConf = config["zones"]

    if streamURI == "":
        cap = cv2.VideoCapture(0)
    else:
        cap = cv2.VideoCapture(streamURI)

    cap.set(3, width)
    cap.set(4, height)
    # cap.set(4,1)

    zones = []
    for zone in zonesConf:
        zones_img = np.zeros((height, width), np.uint8)
        poly = np.array(zone['points'], np.int32)
        cv2.fillConvexPoly(zones_img, poly, 1)
        # zone - [NAME, MASK, OCCUPIED, COUNTER, OCCUPY, FREE]
        zones.append([zone['name'], zones_img, False, 0, zone['occupy'], zone['free']])

    # kernel orig 3, 11
    kernelOp = np.ones((o_kernel, o_kernel), np.uint8)
    kernelCl = np.ones((c_kernel, c_kernel), np.uint8)

    frameArea = height * width
    searchArea = (frameArea / 1000) * thresholdContour

    return cap, zones, skipFames, searchArea, kernelOp, kernelCl, mqttServer, mqttTopic

def watch(config, show, queue):

    global rundirectly

    cap, zones, skipFames, searchArea, kernelOp, kernelCl, mqttServer, mqttTopic = preparation(config)

    fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=True)
    for zone in zones:
        send.send(zone[0], "Camera starting", mqttServer, mqttTopic)

    while (cap.isOpened()):

        _, frame = cap.read()
        frame = fgbg.apply(frame)
        _, frame = cv2.threshold(frame, 200, 255, cv2.THRESH_BINARY)
        frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernelOp)
        frame = cv2.morphologyEx(frame, cv2.MORPH_CLOSE, kernelCl)

        for zone in zones:
            final = cv2.bitwise_and(frame, frame, mask=zone[1])
            _, contours0, hierarchy = cv2.findContours(final, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            nasiel = False

            for cnt in contours0:
                area = cv2.contourArea(cnt)
                if area > searchArea:
                    cv2.drawContours(final, [cnt], 0, (0, 255, 0), 3)
                    nasiel = True
                    break

            # zone - [NAME, MASK, OCCUPIED, COUNTER, OCCUPY, FREE]
            if nasiel:                                          # movement detected
                # print("nasiel ", time.localtime())
                zone[3] += 1
                if zone[2]:                                     # is ocupied
                    if zone[3] > zone[4]:
                        zone[3] = zone[4]
                else:
                    if zone[3] > zone[4]:
                        zone[3] = zone[4]
                        zone[2] = True
                        send.send(zone[0], "Occupied", mqttServer, mqttTopic)

            else:
                # movement not detected
                zone[3] -= zone[4]/zone[5]
                if not zone[2]:                                 # is not occupied
                    if zone[3] < 0:
                        zone[3] = 0
                else:
                    if zone[3] < 0:
                        zone[3] = 0
                        zone[2] = False
                        send.send(zone[0], "Free", mqttServer, mqttTopic)

            # print (zone[0] + "   " + str(zone[3]))

            if show:
                if zone[2]:
                    text = "occupied " + str(zone[3])
                else:
                    text = "free " + str(zone[3])
                cv2.putText(final, text, (1, 10), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), thickness=1)

                if rundirectly:
                    try:
                        cv2.imshow(zone[0], final)
                    except:
                        print('EOF')
                        break
                # else:
                #    print ("send to queue")

        # k = cv2.waitKey(30) & 0xff
        # if k == 27:
        #   break

        i = 0
        while cap.grab() and i < skipFames:
            i += 1

if __name__ == '__main__':
    rundirectly = True
    with open('config.json') as data_file:
        data = json.load(data_file)

    config = data['configuration'][0]
    if (check_config.check(config)):
        watch(config, True, None)


